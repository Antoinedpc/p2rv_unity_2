using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{

    [SerializeField]
    Transform shootOrigin;

    [SerializeField]
    GameObject shootObject;
    
    [SerializeField]
    float shootSpeed;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ShootObject();
    }

    public void ShootObject()
    {
        GameObject obj = (GameObject)Instantiate(shootObject, shootOrigin.position, shootOrigin.rotation);
        obj.SetActive(true);
        obj.GetComponent<Rigidbody>().velocity = shootOrigin.forward * shootSpeed;
    }
}
